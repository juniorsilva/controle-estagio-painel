import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DisciplineDetailComponent } from './discipline-detail.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

const disciplineDetailRoutes: Routes = [
    {
        path: '',
        component: DisciplineDetailComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(disciplineDetailRoutes),
        NgxDatatableModule
    ],
    declarations: [
        DisciplineDetailComponent
    ]
})
export class DisciplineDetailModule {
}