import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IDiscipline, DisciplineService } from '../../../core/api/discipline/client.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AuthClientHelper } from '../../../core/helpers/auth/auth-client.helper';
import { ImageHelper } from '../../../core/helpers/image/image.helper';

@Component({
    selector: 'app-discipline-detail',
    templateUrl: './discipline-detail.component.html',
    styleUrls: ['./discipline-detail.component.css']
})
export class DisciplineDetailComponent implements OnInit {
    public discipline: IDiscipline = {
        id: null,
    };
    @ViewChild(DatatableComponent) table: DatatableComponent;
    public temp;
    public data;
    public value;

    constructor(private route: ActivatedRoute,
                private disciplineService: DisciplineService,
                private router: Router,
                public auth: AuthClientHelper,
                public imageHelper: ImageHelper) {
    }

    ngOnInit() {
        this.getDiscipline();
    }

    getDiscipline() {
        this.disciplineService.get({
            id: this.route.snapshot.paramMap.get('id'),
        })
            .then(
                discipline => {
                    this.discipline = discipline;
                });
    }

    updateFilter(event) {
        const val = event.target.value.toLowerCase();
        // filter our data
        const temp = this.temp.filter(d => {
            return d.name.toLowerCase().indexOf(val) !== -1 || !val;
        });
        // this.units = temp;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    removeDiscipline() {
        this.disciplineService.remove({id: this.discipline.id})
            .then(
                disciplines => {
                    this.router.navigate(['/disciplines']);
                });
    }

    onDeleteDiscipline() {
        if (confirm('Deseja realmente apagar o curso ' + this.discipline.name + '?')) {
            this.removeDiscipline();
        }
    }
}
