import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DisciplineComponent } from './discipline.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { DisciplineService } from '../../core/api/discipline/client.service';

const disciplineRoutes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        component: DisciplineComponent
    },
    {
        path: 'create',
        loadChildren: 'app/components/discipline/discipline-form/discipline-form.module#DisciplineFormModule'
    },
    {
        path: 'update/:id',
        loadChildren: 'app/components/discipline/discipline-form/discipline-form.module#DisciplineFormModule'
    },
    {
        path: 'view/:id',
        loadChildren: 'app/components/discipline/discipline-detail/discipline-detail.module#DisciplineDetailModule'
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(disciplineRoutes),
        NgxDatatableModule
    ],
    declarations: [
        DisciplineComponent
    ],
    providers: [
        DisciplineService
    ]
})
export class DisciplineModule {
}