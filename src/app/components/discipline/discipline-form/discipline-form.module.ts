import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DisciplineFormComponent } from './discipline-form.component';
import { DisciplineService } from '../../../core/api/discipline/client.service';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgUploaderModule } from 'ngx-uploader';

const disciplineFormRoutes: Routes = [
    {
        path: '',
        component: DisciplineFormComponent
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        NgUploaderModule,
        RouterModule.forChild(disciplineFormRoutes),
    ],
    declarations: [
        DisciplineFormComponent
    ],
    providers: [
        DisciplineService
    ]
})
export class DisciplineFormModule {
}