import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DisciplineService } from '../../../core/api/discipline/client.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ImageHelper } from '../../../core/helpers/image/image.helper';

@Component({
    selector: 'app-discipline-form',
    templateUrl: './discipline-form.component.html',
    styleUrls: ['./discipline-form.component.css']
})
export class DisciplineFormComponent implements OnInit {
    public disciplineForm: FormGroup;
    public disciplineID;

    constructor(private formBuilder: FormBuilder,
                private disciplineService: DisciplineService,
                private router: Router,
                public imageHelper: ImageHelper,
                private route: ActivatedRoute) {
        this.disciplineID = this.route.snapshot.paramMap.get('id');
        this.disciplineForm = formBuilder.group({
            'name': [null, Validators.required],
        });
    }

    ngOnInit() {
        this.getDiscipline();
    }

    getDiscipline() {
        if (this.disciplineID) {
            this.disciplineService.get({
                id: this.disciplineID,
                expand: 'sectors,upload'
            })
                .then(
                    discipline => {
                        this.disciplineForm.controls['name'].setValue(discipline.name);
                    });
        }
    }

    onSave() {
        if (this.disciplineID) {
            this.disciplineForm.value.id = this.disciplineID;
            this.disciplineService.update(this.disciplineForm.value)
                .then(
                    res => {
                        this.router.navigate(['/disciplines/view', res.id]);
                    });
        } else {
            this.disciplineService.save(this.disciplineForm.value)
                .then(
                    discipline => {
                        this.router.navigate(['/disciplines/view', discipline.id]);
                    });
        }
    }
}
