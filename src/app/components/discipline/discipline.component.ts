import { Component, OnInit, ViewChild } from '@angular/core';
import { DisciplineService, IDiscipline } from '../../core/api/discipline/client.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AuthClientHelper } from '../../core/helpers/auth/auth-client.helper';
import { ImageHelper } from '../../core/helpers/image/image.helper';

@Component({
    selector: 'app-discipline',
    templateUrl: './discipline.component.html',
    styleUrls: ['./discipline.component.css']
})
export class DisciplineComponent implements OnInit {
    public disciplines: IDiscipline[] = [];
    private temp = [];
    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(private disciplineService: DisciplineService,
                public auth: AuthClientHelper,
                public imageHelper: ImageHelper) {
    }

    ngOnInit() {
        this.getDisciplines();
    }

    getDisciplines() {
        this.disciplineService.query({
            expand: 'upload',
            sort: 'first_name'
        })
            .then(
                disciplines => {
                    this.disciplines = disciplines;
                    this.temp = disciplines;
                });
    }

    removeUnit(discipline) {
        this.disciplineService.remove({id: discipline.id})
            .then(
                disciplines => {
                    this.disciplines.splice(this.disciplines.indexOf(discipline), 1);
                    this.temp = this.disciplines;
                });
    }

    onDeleteDiscipline(id) {
        const found = this.temp.filter(d => {
            return d.id === id;
        });

        if (found.length) {
            if (confirm('Deseja realmente apagar o curso ' + found[0].name + '?')) {
                this.removeUnit(found[0]);
            }
        }

    }
}
