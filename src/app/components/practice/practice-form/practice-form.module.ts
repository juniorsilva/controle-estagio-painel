import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PracticeFormComponent } from './practice-form.component';
import { PracticeService } from '../../../core/api/practice/client.service';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgUploaderModule } from 'ngx-uploader';
import { SelectModule } from 'ng2-select';
import { StudentService } from '../../../core/api/student/client.service';
import { DisciplineService } from '../../../core/api/discipline/client.service';
import { IqDatepickerModule } from 'ngx-iq-datepicker';

const practiceFormRoutes: Routes = [
    {
        path: '',
        component: PracticeFormComponent
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        NgUploaderModule,
        SelectModule,
        IqDatepickerModule,
        RouterModule.forChild(practiceFormRoutes),
    ],
    declarations: [
        PracticeFormComponent
    ],
    providers: [
        PracticeService,
        StudentService,
        DisciplineService
    ]
})
export class PracticeFormModule {
}