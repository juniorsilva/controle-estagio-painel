import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PracticeService } from '../../../core/api/practice/client.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ImageHelper } from '../../../core/helpers/image/image.helper';
import { IStudent, StudentService } from '../../../core/api/student/client.service';
import { DisciplineService, IDiscipline } from '../../../core/api/discipline/client.service';

@Component({
    selector: 'app-practice-form',
    templateUrl: './practice-form.component.html',
    styleUrls: ['./practice-form.component.css']
})
export class PracticeFormComponent implements OnInit {
    public practiceForm: FormGroup;
    public practiceID;
    public students: IStudent[] = [];
    public disciplines: IDiscipline[] = [];

    constructor(private formBuilder: FormBuilder,
                private practiceService: PracticeService,
                private router: Router,
                public imageHelper: ImageHelper,
                private route: ActivatedRoute,
                private disciplineService: DisciplineService,
                private studentService: StudentService) {
        this.practiceID = this.route.snapshot.paramMap.get('id');
        this.practiceForm = formBuilder.group({
            'student_id': [null, Validators.required],
            'discipline_id': [null, Validators.required],
            'local': [null, Validators.required],
            'activity': [null, Validators.required],
            'observation': [null],
            'started_at': [null],
            'ended_at': [null],
            'schedule': [null],
        });
    }

    ngOnInit() {
        this.getPractice();
        this.getDisciplines();
        this.getStudents();
    }

    getStudents() {
        this.studentService.query()
            .then(
                students => {
                    this.students = students.map(student => {
                        return {id: student.id, text: student.name}
                    });
                });
    }

    getDisciplines() {
        this.disciplineService.query()
            .then(
                disciplines => {
                    this.disciplines = disciplines.map(discipline => {
                        return {id: discipline.id, text: discipline.name}
                    });
                });
    }

    getPractice() {
        if (this.practiceID) {
            this.practiceService.get({
                id: this.practiceID,
            })
                .then(
                    practice => {
                        this.practiceForm.controls['student_id'].setValue(practice.student_id);
                        this.practiceForm.controls['discipline_id'].setValue(practice.discipline_id);
                        this.practiceForm.controls['local'].setValue(practice.local);
                        this.practiceForm.controls['activity'].setValue(practice.activity);
                        this.practiceForm.controls['observation'].setValue(practice.observation);
                        this.practiceForm.controls['started_at'].setValue(practice.started_at);
                        this.practiceForm.controls['ended_at'].setValue(practice.ended_at);
                        this.practiceForm.controls['schedule'].setValue(practice.schedule);
                    });
        }
    }

    onSave() {
        if (this.practiceID) {
            this.practiceForm.value.id = this.practiceID;
            this.practiceService.update(this.practiceForm.value)
                .then(
                    res => {
                        this.router.navigate(['/practices/view', res.id]);
                    });
        } else {
            this.practiceService.save(this.practiceForm.value)
                .then(
                    practice => {
                        this.router.navigate(['/practices/view', practice.id]);
                    });
        }
    }

    onRefreshValue(event) {
        if (event)
            this.practiceForm.controls['student_id'].setValue(event.id);
        else
            this.practiceForm.controls['student_id'].setValue(null);
    }

    onRefreshDisciplineValue(event) {
        if (event)
            this.practiceForm.controls['discipline_id'].setValue(event.id);
        else
            this.practiceForm.controls['discipline_id'].setValue(null);
    }
}
