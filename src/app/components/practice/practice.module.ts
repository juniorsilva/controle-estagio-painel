import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PracticeComponent } from './practice.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { PracticeService } from '../../core/api/practice/client.service';
import { CommonModule } from '@angular/common';

const practiceRoutes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        component: PracticeComponent
    },
    {
        path: 'create',
        loadChildren: 'app/components/practice/practice-form/practice-form.module#PracticeFormModule'
    },
    {
        path: 'update/:id',
        loadChildren: 'app/components/practice/practice-form/practice-form.module#PracticeFormModule'
    },
    {
        path: 'view/:id',
        loadChildren: 'app/components/practice/practice-detail/practice-detail.module#PracticeDetailModule'
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(practiceRoutes),
        NgxDatatableModule
    ],
    declarations: [
        PracticeComponent
    ],
    providers: [
        PracticeService
    ]
})
export class PracticeModule {
}