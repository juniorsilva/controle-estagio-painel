import { Component, OnInit, ViewChild } from '@angular/core';
import { PracticeService, IPractice } from '../../core/api/practice/client.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AuthClientHelper } from '../../core/helpers/auth/auth-client.helper';
import { ImageHelper } from '../../core/helpers/image/image.helper';

@Component({
    selector: 'app-practice',
    templateUrl: './practice.component.html',
    styleUrls: ['./practice.component.css']
})
export class PracticeComponent implements OnInit {
    public practices: IPractice[] = [];
    private temp = [];
    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(private practiceService: PracticeService,
                public auth: AuthClientHelper,
                public imageHelper: ImageHelper) {
    }

    ngOnInit() {
        this.getPractices();
    }

    getPractices() {
        this.practiceService.query({
            expand: 'student,discipline'
        })
            .then(
                practices => {
                    this.practices = practices;
                    this.temp = practices;
                });
    }

    removeUnit(practice) {
        this.practiceService.remove({id: practice.id})
            .then(
                practices => {
                    this.practices.splice(this.practices.indexOf(practice), 1);
                    this.temp = this.practices;
                });
    }

    onDeletePractice(id) {
        const found = this.temp.filter(d => {
            return d.id === id;
        });

        if (found.length) {
            if (confirm('Deseja realmente apagar o curso ' + found[0].name + '?')) {
                this.removeUnit(found[0]);
            }
        }

    }
}
