import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IPractice, PracticeService } from '../../../core/api/practice/client.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AuthClientHelper } from '../../../core/helpers/auth/auth-client.helper';
import { ImageHelper } from '../../../core/helpers/image/image.helper';

@Component({
    selector: 'app-practice-detail',
    templateUrl: './practice-detail.component.html',
    styleUrls: ['./practice-detail.component.css']
})
export class PracticeDetailComponent implements OnInit {
    public practice: IPractice = {
        id: null,
        discipline: [],
        student: [],
    };
    @ViewChild(DatatableComponent) table: DatatableComponent;
    public temp;
    public data;
    public value;

    constructor(private route: ActivatedRoute,
                private practiceService: PracticeService,
                private router: Router,
                public auth: AuthClientHelper,
                public imageHelper: ImageHelper) {
    }

    ngOnInit() {
        this.getPractice();
    }

    getPractice() {
        this.practiceService.get({
            id: this.route.snapshot.paramMap.get('id'),
            expand: 'student,discipline'
        })
            .then(
                practice => {
                    this.practice = practice;
                });
    }

    updateFilter(event) {
        const val = event.target.value.toLowerCase();
        // filter our data
        const temp = this.temp.filter(d => {
            return d.name.toLowerCase().indexOf(val) !== -1 || !val;
        });
        // this.units = temp;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    removePractice() {
        this.practiceService.remove({id: this.practice.id})
            .then(
                practices => {
                    this.router.navigate(['/practices']);
                });
    }

    onDeletePractice() {
        if (confirm('Deseja realmente apagar o curso ' + this.practice.name + '?')) {
            this.removePractice();
        }
    }
}
