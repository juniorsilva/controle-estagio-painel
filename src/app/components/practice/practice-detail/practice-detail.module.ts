import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PracticeDetailComponent } from './practice-detail.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { CommonModule } from '@angular/common';

const practiceDetailRoutes: Routes = [
    {
        path: '',
        component: PracticeDetailComponent
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(practiceDetailRoutes),
        NgxDatatableModule
    ],
    declarations: [
        PracticeDetailComponent
    ]
})
export class PracticeDetailModule {
}