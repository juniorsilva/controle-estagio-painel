import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CourseDetailComponent } from './course-detail.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SelectModule } from 'ng2-select';
import { DisciplineService } from '../../../core/api/discipline/client.service';
import { CommonModule } from '@angular/common';

const courseDetailRoutes: Routes = [
    {
        path: '',
        component: CourseDetailComponent
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(courseDetailRoutes),
        SelectModule,
        NgxDatatableModule
    ],
    declarations: [
        CourseDetailComponent,
    ],
    providers: [
        DisciplineService
    ]
})
export class CourseDetailModule {
}