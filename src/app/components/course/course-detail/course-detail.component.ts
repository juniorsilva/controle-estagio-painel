import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ICourse, CourseService } from '../../../core/api/course/client.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AuthClientHelper } from '../../../core/helpers/auth/auth-client.helper';
import { ImageHelper } from '../../../core/helpers/image/image.helper';
import { DisciplineService } from '../../../core/api/discipline/client.service';

@Component({
    selector: 'app-course-detail',
    templateUrl: './course-detail.component.html',
    styleUrls: ['./course-detail.component.css']
})
export class CourseDetailComponent implements OnInit {
    public course: ICourse = {
        id: null,
        disciplines: []
    };
    @ViewChild(DatatableComponent) table: DatatableComponent;
    public temp;
    public data;
    public value;
    public disciplines = [];
    public disciplinesSelected = [];

    constructor(private route: ActivatedRoute,
                private courseService: CourseService,
                private disciplineService: DisciplineService,
                private router: Router,
                public auth: AuthClientHelper,
                public imageHelper: ImageHelper) {
    }

    ngOnInit() {
        this.getCourse();
    }

    getDisciplines() {
        this.disciplineService.query()
            .then(
                disciplines => {
                    this.disciplines = disciplines.filter(discipline => {
                        let found = false;
                        this.course.disciplines.forEach(item => {
                            if (item.id == discipline.id)
                                found = true;
                        });

                        return !found;
                    });

                    this.disciplines = this.disciplines.map(discipline => {
                        return {id: discipline.id, text: discipline.name};
                    });
                });
    }

    getCourse() {
        this.courseService.get({
            id: this.route.snapshot.paramMap.get('id'),
            expand: 'disciplines'
        })
            .then(
                course => {
                    this.course = course;
                    this.getDisciplines();
                });
    }

    updateFilter(event) {
        const val = event.target.value.toLowerCase();
        // filter our data
        const temp = this.temp.filter(d => {
            return d.name.toLowerCase().indexOf(val) !== -1 || !val;
        });
        // this.units = temp;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    removeCourse() {
        this.courseService.remove({id: this.course.id})
            .then(
                courses => {
                    this.router.navigate(['/courses']);
                });
    }

    onDeleteCourse() {
        if (confirm('Deseja realmente apagar o curso ' + this.course.name + '?')) {
            this.removeCourse();
        }
    }

    onRefreshValue(event) {
        this.disciplinesSelected = event;
    }

    onAddDisciplines() {
        this.courseService.addDisciplines({
            id: this.route.snapshot.paramMap.get('id'),
            disciplines: this.disciplinesSelected
        })
            .then(
                course => {
                    this.getCourse();
                    this.value = [];
                });
    }

    onRemoveDiscipline(discipline) {
        if (confirm('Deseja realmente remover a disciplina "' + discipline.name + '" deste curso?')) {
            this.courseService.removeDiscipline({
                id: this.route.snapshot.paramMap.get('id'),
                discipline_id: discipline.id
            })
                .then(
                    course => {
                        this.getCourse();
                    });
        }
    }
}
