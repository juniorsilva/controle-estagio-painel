import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CourseFormComponent } from './course-form.component';
import { CourseService } from '../../../core/api/course/client.service';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgUploaderModule } from 'ngx-uploader';

const courseFormRoutes: Routes = [
    {
        path: '',
        component: CourseFormComponent
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        NgUploaderModule,
        RouterModule.forChild(courseFormRoutes),
    ],
    declarations: [
        CourseFormComponent
    ],
    providers: [
        CourseService
    ]
})
export class CourseFormModule {
}