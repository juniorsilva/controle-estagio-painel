import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CourseService } from '../../../core/api/course/client.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ImageHelper } from '../../../core/helpers/image/image.helper';
import { AuthClientHelper } from '../../../core/helpers/auth/auth-client.helper';

@Component({
    selector: 'app-course-form',
    templateUrl: './course-form.component.html',
    styleUrls: ['./course-form.component.css']
})
export class CourseFormComponent implements OnInit {
    public courseForm: FormGroup;
    public courseID;

    constructor(private formBuilder: FormBuilder,
                private courseService: CourseService,
                private router: Router,
                public imageHelper: ImageHelper,
                private route: ActivatedRoute) {
        this.courseID = this.route.snapshot.paramMap.get('id');
        this.courseForm = formBuilder.group({
            'name': [null, Validators.required],
            'code': [null, Validators.required],
            'period': [null, Validators.required],
        });
    }

    ngOnInit() {
        this.getCourse();
    }

    getCourse() {
        if (this.courseID) {
            this.courseService.get({
                id: this.courseID,
                expand: 'sectors,upload'
            })
                .then(
                    course => {
                        this.courseForm.controls['name'].setValue(course.name);
                        this.courseForm.controls['code'].setValue(course.code);
                        this.courseForm.controls['period'].setValue(course.period);
                    });
        }
    }

    onSave() {
        if (this.courseID) {
            this.courseForm.value.id = this.courseID;
            this.courseService.update(this.courseForm.value)
                .then(
                    res => {
                        this.router.navigate(['/courses/view', res.id]);
                    });
        } else {
            this.courseService.save(this.courseForm.value)
                .then(
                    course => {
                        this.router.navigate(['/courses/view', course.id]);
                    });
        }
    }
}
