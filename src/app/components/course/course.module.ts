import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CourseComponent } from './course.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { CourseService } from '../../core/api/course/client.service';

const courseRoutes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        component: CourseComponent
    },
    {
        path: 'create',
        loadChildren: 'app/components/course/course-form/course-form.module#CourseFormModule'
    },
    {
        path: 'update/:id',
        loadChildren: 'app/components/course/course-form/course-form.module#CourseFormModule'
    },
    {
        path: 'view/:id',
        loadChildren: 'app/components/course/course-detail/course-detail.module#CourseDetailModule'
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(courseRoutes),
        NgxDatatableModule
    ],
    declarations: [
        CourseComponent
    ],
    providers:[
        CourseService
    ]
})
export class CourseModule {
}