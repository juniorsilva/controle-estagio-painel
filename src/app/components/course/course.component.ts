import { Component, OnInit, ViewChild } from '@angular/core';
import { CourseService, ICourse } from '../../core/api/course/client.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AuthClientHelper } from '../../core/helpers/auth/auth-client.helper';
import { ImageHelper } from '../../core/helpers/image/image.helper';

@Component({
    selector: 'app-course',
    templateUrl: './course.component.html',
    styleUrls: ['./course.component.css']
})
export class CourseComponent implements OnInit {
    public courses: ICourse[] = [];
    private temp = [];
    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(private courseService: CourseService,
                public auth: AuthClientHelper,
                public imageHelper: ImageHelper) {
    }

    ngOnInit() {
        this.getCourses();
    }

    getCourses() {
        this.courseService.query({
            expand: 'upload',
            sort: 'first_name'
        })
            .then(
                courses => {
                    this.courses = courses;
                    this.temp = courses;
                });
    }

    removeUnit(course) {
        this.courseService.remove({id: course.id})
            .then(
                courses => {
                    this.courses.splice(this.courses.indexOf(course), 1);
                    this.temp = this.courses;
                });
    }

    onDeleteCourse(id) {
        const found = this.temp.filter(d => {
            return d.id === id;
        });

        if (found.length) {
            if (confirm('Deseja realmente apagar o curso ' + found[0].name + '?')) {
                this.removeUnit(found[0]);
            }
        }

    }
}
