import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../core/api/user/client.service';
import { AuthClientHelper } from '../../core/helpers/auth/auth-client.helper';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent {
    loginForm: FormGroup;

    constructor(private router: Router,
                private formBuilder: FormBuilder,
                private userService: UserService,
                private auth: AuthClientHelper) {
        this.loginForm = formBuilder.group({
            'email': [null, [Validators.email, Validators.required]],
            'password': [null, Validators.required],
        });
    }

    onLogin() {
        this.userService.login(this.loginForm.value)
            .then(user => {
                this.auth.setUser({
                    id: user.id,
                    first_name: user.first_name,
                    last_name: user.last_name,
                    access_token: user.access_token,
                    user_type: user.user_type
                });
                this.router.navigate(['/home']);
            });
    }
}
