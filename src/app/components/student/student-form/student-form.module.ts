import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StudentFormComponent } from './student-form.component';
import { StudentService } from '../../../core/api/student/client.service';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgUploaderModule } from 'ngx-uploader';

const studentFormRoutes: Routes = [
    {
        path: '',
        component: StudentFormComponent
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        NgUploaderModule,
        RouterModule.forChild(studentFormRoutes),
    ],
    declarations: [
        StudentFormComponent
    ],
    providers: [
        StudentService
    ]
})
export class StudentFormModule {
}