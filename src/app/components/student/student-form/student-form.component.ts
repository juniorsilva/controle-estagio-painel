import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { StudentService } from '../../../core/api/student/client.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ImageHelper } from '../../../core/helpers/image/image.helper';

@Component({
    selector: 'app-student-form',
    templateUrl: './student-form.component.html',
    styleUrls: ['./student-form.component.css']
})
export class StudentFormComponent implements OnInit {
    public studentForm: FormGroup;
    public studentID;

    constructor(private formBuilder: FormBuilder,
                private studentService: StudentService,
                private router: Router,
                public imageHelper: ImageHelper,
                private route: ActivatedRoute) {
        this.studentID = this.route.snapshot.paramMap.get('id');
        this.studentForm = formBuilder.group({
            'name': [null, Validators.required],
            'matriculation': [null, Validators.required],
            'period': [null, Validators.required],
        });
    }

    ngOnInit() {
        this.getStudent();
    }

    getStudent() {
        if (this.studentID) {
            this.studentService.get({
                id: this.studentID,
                expand: 'sectors,upload'
            })
                .then(
                    student => {
                        this.studentForm.controls['name'].setValue(student.name);
                        this.studentForm.controls['matriculation'].setValue(student.matriculation);
                        this.studentForm.controls['period'].setValue(student.period);
                    });
        }
    }

    onSave() {
        if (this.studentID) {
            this.studentForm.value.id = this.studentID;
            this.studentService.update(this.studentForm.value)
                .then(
                    res => {
                        this.router.navigate(['/students/view', res.id]);
                    });
        } else {
            this.studentService.save(this.studentForm.value)
                .then(
                    student => {
                        this.router.navigate(['/students/view', student.id]);
                    });
        }
    }
}
