import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IStudent, StudentService } from '../../../core/api/student/client.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AuthClientHelper } from '../../../core/helpers/auth/auth-client.helper';
import { ImageHelper } from '../../../core/helpers/image/image.helper';

@Component({
    selector: 'app-student-detail',
    templateUrl: './student-detail.component.html',
    styleUrls: ['./student-detail.component.css']
})
export class StudentDetailComponent implements OnInit {
    public student: IStudent = {
        id: null,
    };
    @ViewChild(DatatableComponent) table: DatatableComponent;
    public temp;
    public data;
    public value;

    constructor(private route: ActivatedRoute,
                private studentService: StudentService,
                private router: Router,
                public auth: AuthClientHelper,
                public imageHelper: ImageHelper) {
    }

    ngOnInit() {
        this.getStudent();
    }

    getStudent() {
        this.studentService.get({
            id: this.route.snapshot.paramMap.get('id'),
        })
            .then(
                student => {
                    this.student = student;
                });
    }

    updateFilter(event) {
        const val = event.target.value.toLowerCase();
        // filter our data
        const temp = this.temp.filter(d => {
            return d.name.toLowerCase().indexOf(val) !== -1 || !val;
        });
        // this.units = temp;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    removeStudent() {
        this.studentService.remove({id: this.student.id})
            .then(
                students => {
                    this.router.navigate(['/students']);
                });
    }

    onDeleteStudent() {
        if (confirm('Deseja realmente apagar o estudante ' + this.student.name + '?')) {
            this.removeStudent();
        }
    }
}
