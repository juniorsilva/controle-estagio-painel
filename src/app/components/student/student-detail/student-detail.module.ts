import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StudentDetailComponent } from './student-detail.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

const studentDetailRoutes: Routes = [
    {
        path: '',
        component: StudentDetailComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(studentDetailRoutes),
        NgxDatatableModule
    ],
    declarations: [
        StudentDetailComponent
    ]
})
export class StudentDetailModule {
}