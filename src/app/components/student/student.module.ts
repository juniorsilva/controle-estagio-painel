import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StudentComponent } from './student.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { StudentService } from '../../core/api/student/client.service';

const studentRoutes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        component: StudentComponent
    },
    {
        path: 'create',
        loadChildren: 'app/components/student/student-form/student-form.module#StudentFormModule'
    },
    {
        path: 'update/:id',
        loadChildren: 'app/components/student/student-form/student-form.module#StudentFormModule'
    },
    {
        path: 'view/:id',
        loadChildren: 'app/components/student/student-detail/student-detail.module#StudentDetailModule'
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(studentRoutes),
        NgxDatatableModule
    ],
    declarations: [
        StudentComponent
    ],
    providers: [
        StudentService
    ]
})
export class StudentModule {
}