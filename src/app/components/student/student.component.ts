import { Component, OnInit, ViewChild } from '@angular/core';
import { StudentService, IStudent } from '../../core/api/student/client.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AuthClientHelper } from '../../core/helpers/auth/auth-client.helper';
import { ImageHelper } from '../../core/helpers/image/image.helper';

@Component({
    selector: 'app-student',
    templateUrl: './student.component.html',
    styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {
    public students: IStudent[] = [];
    private temp = [];
    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(private studentService: StudentService,
                public auth: AuthClientHelper,
                public imageHelper: ImageHelper) {
    }

    ngOnInit() {
        this.getStudents();
    }

    getStudents() {
        this.studentService.query({
            expand: 'upload',
            sort: 'first_name'
        })
            .then(
                students => {
                    this.students = students;
                    this.temp = students;
                });
    }

    removeUnit(student) {
        this.studentService.remove({id: student.id})
            .then(
                students => {
                    this.students.splice(this.students.indexOf(student), 1);
                    this.temp = this.students;
                });
    }

    onDeleteStudent(id) {
        const found = this.temp.filter(d => {
            return d.id === id;
        });

        if (found.length) {
            if (confirm('Deseja realmente apagar o estudante ' + found[0].name + '?')) {
                this.removeUnit(found[0]);
            }
        }

    }
}
