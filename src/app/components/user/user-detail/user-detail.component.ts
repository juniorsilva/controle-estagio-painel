import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IUser, UserService } from '../../../core/api/user/client.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AuthClientHelper } from '../../../core/helpers/auth/auth-client.helper';
import { ImageHelper } from '../../../core/helpers/image/image.helper';

@Component({
    selector: 'app-user-detail',
    templateUrl: './user-detail.component.html',
    styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {
    public user: IUser = {
        id: null,
        professor: []
    };
    @ViewChild(DatatableComponent) table: DatatableComponent;
    public temp;
    public data;
    public value;

    constructor(private route: ActivatedRoute,
                private userService: UserService,
                private router: Router,
                public auth: AuthClientHelper,
                public imageHelper: ImageHelper) {
    }

    ngOnInit() {
        this.getUser();
    }

    getUser() {
        this.userService.get({
            id: this.route.snapshot.paramMap.get('id'),
            expand: 'professor'
        })
            .then(
                user => {
                    this.user = user;
                });
    }

    updateFilter(event) {
        const val = event.target.value.toLowerCase();
        // filter our data
        const temp = this.temp.filter(d => {
            return d.name.toLowerCase().indexOf(val) !== -1 || !val;
        });
        // this.units = temp;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    removeUser() {
        this.userService.remove({id: this.user.id})
            .then(
                users => {
                    this.router.navigate(['/users']);
                });
    }

    onDeleteUser() {
        if (confirm('Deseja realmente apagar o usuário ' + this.user.first_name + '?')) {
            this.removeUser();
        }
    }
}
