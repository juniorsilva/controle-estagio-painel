import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserDetailComponent } from './user-detail.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { CommonModule } from '@angular/common';

const userDetailRoutes: Routes = [
    {
        path: '',
        component: UserDetailComponent
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(userDetailRoutes),
        NgxDatatableModule
    ],
    declarations: [
        UserDetailComponent
    ]
})
export class UserDetailModule {
}