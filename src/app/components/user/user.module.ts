import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserComponent } from './user.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

const userRoutes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        component: UserComponent
    },
    {
        path: 'create',
        loadChildren: 'app/components/user/user-form/user-form.module#UserFormModule'
    },
    {
        path: 'update/:id',
        loadChildren: 'app/components/user/user-form/user-form.module#UserFormModule'
    },
    {
        path: 'view/:id',
        loadChildren: 'app/components/user/user-detail/user-detail.module#UserDetailModule'
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(userRoutes),
        NgxDatatableModule
    ],
    declarations: [
        UserComponent
    ]
})
export class UserModule {
}