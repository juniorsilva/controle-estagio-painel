import { Component, OnInit, ViewChild } from '@angular/core';
import { IUser, UserService } from '../../core/api/user/client.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AuthClientHelper } from '../../core/helpers/auth/auth-client.helper';
import { ImageHelper } from '../../core/helpers/image/image.helper';

@Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
    public users: IUser[] = [];
    private temp = [];
    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(private userService: UserService,
                public auth: AuthClientHelper,
                public imageHelper: ImageHelper) {
    }

    ngOnInit() {
        this.getUsers();
    }

    getUsers() {
        this.userService.query({
            expand: 'upload',
            sort: 'first_name'
        })
            .then(
                users => {
                    this.users = users;
                    this.temp = users;
                });
    }

    removeUnit(user) {
        this.userService.remove({id: user.id})
            .then(
                users => {
                    this.users.splice(this.users.indexOf(user), 1);
                    this.temp = this.users;
                });
    }

    onDeleteUser(id) {
        const found = this.temp.filter(d => {
            return d.id === id;
        });

        if (found.length) {
            if (confirm('Deseja realmente apagar o usuário ' + found[0].first_name + '?')) {
                this.removeUnit(found[0]);
            }
        }

    }
}
