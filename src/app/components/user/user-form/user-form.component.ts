import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../../core/api/user/client.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ImageHelper } from '../../../core/helpers/image/image.helper';
import { AuthClientHelper } from '../../../core/helpers/auth/auth-client.helper';

@Component({
    selector: 'app-user-form',
    templateUrl: './user-form.component.html',
    styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {
    public userForm: FormGroup;
    public userID;

    constructor(private formBuilder: FormBuilder,
                private userService: UserService,
                private router: Router,
                public imageHelper: ImageHelper,
                private route: ActivatedRoute,
                private auth: AuthClientHelper) {
        this.userID = this.route.snapshot.paramMap.get('id');
        this.userForm = formBuilder.group({
            'first_name': [null, Validators.required],
            'last_name': [null, Validators.required],
            'email': [null, [Validators.required, Validators.email]],
            'password': [null, (this.userID) ? '' : Validators.required],
            'user_type': [1, Validators.required],
            'matriculation': [null],
        });
    }

    ngOnInit() {
        this.getUser();
    }

    getUser() {
        if (this.userID) {
            this.userService.get({
                id: this.userID,
                expand: 'professor'
            })
                .then(
                    user => {
                        this.userForm.controls['first_name'].setValue(user.first_name);
                        this.userForm.controls['last_name'].setValue(user.last_name);
                        this.userForm.controls['email'].setValue(user.email);
                        this.userForm.controls['user_type'].setValue(user.user_type);
                        if (user.professor)
                            this.userForm.controls['matriculation'].setValue(user.professor.matriculation);
                    });
        }
    }

    onSave() {
        if (this.userID) {
            this.userForm.value.id = this.userID;
            this.userService.update(this.userForm.value)
                .then(
                    res => {
                        if (this.userID == this.auth.getUser().id) {
                            this.userService.get({
                                id: this.userID,
                            })
                                .then(
                                    user => {
                                        this.auth.setUser({
                                            id: user.id,
                                            first_name: user.first_name,
                                            last_name: user.last_name,
                                            access_token: user.access_token,
                                            user_type: user.user_type
                                        });
                                    });
                        }
                        this.router.navigate(['/users/view', res.id]);
                    });
        } else {
            this.userService.save(this.userForm.value)
                .then(
                    user => {
                        this.router.navigate(['/users/view', user.id]);
                    });
        }
    }

    checkUser() {
        return (this.userForm.value.user_type == 1);
    }
}
