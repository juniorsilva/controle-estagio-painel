import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserFormComponent } from './user-form.component';
import { UserService } from '../../../core/api/user/client.service';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgUploaderModule } from 'ngx-uploader';

const userFormRoutes: Routes = [
    {
        path: '',
        component: UserFormComponent
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        NgUploaderModule,
        RouterModule.forChild(userFormRoutes),
    ],
    declarations: [
        UserFormComponent
    ],
    providers: [
        UserService
    ]
})
export class UserFormModule {
}