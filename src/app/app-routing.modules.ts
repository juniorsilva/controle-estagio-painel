import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from './core/helpers/auth/auth-guard.helper';

const appRoutes: Routes = [
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
    },
    {
        path: 'login',
        runGuardsAndResolvers: 'always',
        canActivate: [AuthGuardService],
        loadChildren: 'app/components/login/login.module#LoginModule'
    },
    {
        path: 'home',
        runGuardsAndResolvers: 'always',
        canActivate: [AuthGuardService],
        loadChildren: 'app/components/home/home.module#HomeModule'
    },
    {
        path: 'users',
        runGuardsAndResolvers: 'always',
        canActivate: [AuthGuardService],
        loadChildren: 'app/components/user/user.module#UserModule'
    },
    {
        path: 'courses',
        runGuardsAndResolvers: 'always',
        canActivate: [AuthGuardService],
        loadChildren: 'app/components/course/course.module#CourseModule'
    },
    {
        path: 'disciplines',
        runGuardsAndResolvers: 'always',
        canActivate: [AuthGuardService],
        loadChildren: 'app/components/discipline/discipline.module#DisciplineModule'
    },
    {
        path: 'students',
        runGuardsAndResolvers: 'always',
        canActivate: [AuthGuardService],
        loadChildren: 'app/components/student/student.module#StudentModule'
    },
    {
        path: 'practices',
        runGuardsAndResolvers: 'always',
        canActivate: [AuthGuardService],
        loadChildren: 'app/components/practice/practice.module#PracticeModule'
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(
            appRoutes,
            {enableTracing: false}
        )
    ],
    exports: [RouterModule],
    providers: [
        AuthGuardService
    ]
})

export class AppRoutingModules {
}