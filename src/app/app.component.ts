import {AfterViewChecked, Component, OnInit} from '@angular/core';
import {AuthClientHelper} from './core/helpers/auth/auth-client.helper';
import {Router} from '@angular/router';
import {SpinnerHelper} from './core/helpers/spinner/spinner.helper';
import {ImageHelper} from './core/helpers/image/image.helper';

declare function setContentHeight();

@Component({
    selector: 'app-component',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterViewChecked {
    public objLoaderStatus;
    public user;
    public isLogged;

    constructor(private auth: AuthClientHelper,
                private router: Router,
                public imageHelper: ImageHelper) {
        this.objLoaderStatus = false;
    }

    ngAfterViewChecked() {
        setContentHeight();
    }

    ngOnInit() {
        this.auth.isLogged.subscribe((val) => {
            this.isLogged = val;
        });

        this.auth.user.subscribe((val) => {
            this.user = val;
        });

        SpinnerHelper.loaderStatus.subscribe((val) => {
            this.objLoaderStatus = val;
        });

        if (this.user) {
            this.auth.setUserStatus(true);
        }
    }

    logout() {
        this.router.navigate(['/login']);
        this.auth.clear();
    }
}
