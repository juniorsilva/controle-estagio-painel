import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AppRoutingModules } from './app-routing.modules';
import { Ng2Webstorage } from 'ngx-webstorage';
import { ReactiveFormsModule } from '@angular/forms';
import { UserService } from './core/api/user/client.service';
import { AuthClientHelper } from './core/helpers/auth/auth-client.helper';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { CURRENCY_MASK_CONFIG, CurrencyMaskConfig } from 'ng2-currency-mask/src/currency-mask.config';
import { TruncatePipe } from './core/pipes/truncate/truncate.pipe';
import { NgUploaderModule } from 'ngx-uploader';
import { ImageHelper } from './core/helpers/image/image.helper';
import { SpinnerHelper } from './core/helpers/spinner/spinner.helper';
import { registerLocaleData } from '@angular/common';
import localePT from '@angular/common/locales/pt';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { RestModule } from 'rest-ngx';

registerLocaleData(localePT);

export const CustomCurrencyMaskConfig: CurrencyMaskConfig = {
    align: 'right',
    allowNegative: true,
    allowZero: true,
    decimal: ',',
    precision: 2,
    prefix: 'R$ ',
    suffix: '',
    thousands: '.'
};

@NgModule({
    declarations: [
        TruncatePipe,
        /** Components **/
        AppComponent
    ],
    imports: [
        BrowserModule,
        Ng2Webstorage.forRoot({
            prefix: 'controleEstagio'
        }),
        ReactiveFormsModule,
        HttpClientModule,
        NgxDatatableModule,
        CurrencyMaskModule,
        NgUploaderModule,
        RestModule.forRoot(),
        /** Routes **/
        AppRoutingModules
    ],
    providers: [
        UserService,
        AuthClientHelper,
        ImageHelper,
        SpinnerHelper,
        {provide: CURRENCY_MASK_CONFIG, useValue: CustomCurrencyMaskConfig},
        {provide: LOCALE_ID, useValue: 'pt'},
        {provide: HTTP_INTERCEPTORS, useClass: AuthClientHelper, multi: true}
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
