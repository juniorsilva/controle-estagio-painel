import { Injectable } from '@angular/core';
import { AuthClientHelper } from '../helpers/auth/auth-client.helper';
import { environment } from '../../../environments/environment';
import { Rest, RestHandler } from 'rest-core';

@Injectable()
export class BaseService extends Rest {
    public static BASE_URL = environment.apiUrl;

    constructor(restHandler: RestHandler, private _auth: AuthClientHelper) {
        super(restHandler);
    }

    $getHeaders(methodOptions: any): any {
        let headers = super.$getHeaders();

        // Extending our headers with Authorization
        if (!methodOptions.skipAuthorization) {
            headers = this._auth.extendHeaders(headers);
        }

        return headers;
    }

}
