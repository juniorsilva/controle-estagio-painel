import { Injectable } from '@angular/core';
import { BaseService } from '../base.service';
import { IRestMethod, RestAction, RestParams, RestRequestMethod } from 'rest-core';

/**
 Class for the DisciplineService.
 */

export interface IDiscipline {
    id?: number;
    name?: string;
    period?: number;
    code?: string;
}

@Injectable()
@RestParams({
    url: BaseService.BASE_URL + '/v1/disciplines'
})
export class DisciplineService extends BaseService {
    @RestAction({
        method: RestRequestMethod.Post,
    })
    save: IRestMethod<any, any>;

    @RestAction({
        method: RestRequestMethod.Put,
        path: '/{!id}'
    })
    update: IRestMethod<any, any>;

    @RestAction({
        path: '/{!id}'
    })
    get: IRestMethod<any, any>;

    @RestAction({
        isArray: true
    })
    query: IRestMethod<any, any[]>;

    @RestAction({
        method: RestRequestMethod.Delete,
        path: '/{!id}'
    })
    remove: IRestMethod<{ id: any }, any>;
}