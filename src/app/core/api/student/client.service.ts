import { Injectable } from '@angular/core';
import { BaseService } from '../base.service';
import { IRestMethod, RestAction, RestParams, RestRequestMethod } from 'rest-core';

/**
 Class for the StudentService.
 */

export interface IStudent {
    id?: number;
    name?: string;
    period?: number;
    matriculation?: string;
}

@Injectable()
@RestParams({
    url: BaseService.BASE_URL + '/v1/students'
})
export class StudentService extends BaseService {
    @RestAction({
        method: RestRequestMethod.Post,
    })
    save: IRestMethod<any, any>;

    @RestAction({
        method: RestRequestMethod.Put,
        path: '/{!id}'
    })
    update: IRestMethod<any, any>;

    @RestAction({
        path: '/{!id}'
    })
    get: IRestMethod<any, any>;

    @RestAction({
        isArray: true
    })
    query: IRestMethod<any, any[]>;

    @RestAction({
        method: RestRequestMethod.Delete,
        path: '/{!id}'
    })
    remove: IRestMethod<{ id: any }, any>;
}