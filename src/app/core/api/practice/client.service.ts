import { Injectable } from '@angular/core';
import { BaseService } from '../base.service';
import { IRestMethod, RestAction, RestParams, RestRequestMethod } from 'rest-core';

/**
 Class for the PracticeService.
 */

export interface IPractice {
    id?: number;
    name?: string;
    period?: number;
    code?: string;
    started_at?: string;
    ended_at?: string;
    observation?: string;
    schedule?: string;
    activity?: string;
    local?: string;
    student?: any;
    discipline?: any;
}

@Injectable()
@RestParams({
    url: BaseService.BASE_URL + '/v1/practices'
})
export class PracticeService extends BaseService {
    @RestAction({
        method: RestRequestMethod.Post,
    })
    save: IRestMethod<any, any>;

    @RestAction({
        method: RestRequestMethod.Put,
        path: '/{!id}'
    })
    update: IRestMethod<any, any>;

    @RestAction({
        path: '/{!id}'
    })
    get: IRestMethod<any, any>;

    @RestAction({
        isArray: true
    })
    query: IRestMethod<any, any[]>;

    @RestAction({
        method: RestRequestMethod.Delete,
        path: '/{!id}'
    })
    remove: IRestMethod<{ id: any }, any>;
}