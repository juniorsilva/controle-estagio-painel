import { Injectable } from '@angular/core';
import { BaseService } from '../base.service';
import { IRestMethod, RestAction, RestParams, RestRequestMethod } from 'rest-core';

/**
 Class for the UserService.
 */

export interface IUser {
    id?: number;
    first_name?: string;
    last_name?: string;
    email?: string;
    access_token?: string;
    user_type?: string;
    professor?: any;
}

@Injectable()
@RestParams({
    url: BaseService.BASE_URL + '/v1/users'
})
export class UserService extends BaseService {
    @RestAction({
        method: RestRequestMethod.Post,
        path: '/login',
        skipAuthorization: true
    })
    login: IRestMethod<{ email: string, password: string }, IUser>;

    @RestAction({
        method: RestRequestMethod.Post,
    })
    save: IRestMethod<any, any>;

    @RestAction({
        method: RestRequestMethod.Put,
        path: '/{!id}'
    })
    update: IRestMethod<any, any>;

    @RestAction({
        path: '/{!id}'
    })
    get: IRestMethod<any, any>;

    @RestAction({
        isArray: true
    })
    query: IRestMethod<any, any[]>;

    @RestAction({
        method: RestRequestMethod.Delete,
        path: '/{!id}'
    })
    remove: IRestMethod<{ id: any }, any>;
}