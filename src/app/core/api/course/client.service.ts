import { Injectable } from '@angular/core';
import { BaseService } from '../base.service';
import { IRestMethod, RestAction, RestParams, RestRequestMethod } from 'rest-core';

/**
 Class for the CourseService.
 */

export interface ICourse {
    id?: number;
    name?: string;
    period?: number;
    code?: string;
    disciplines?: any
}

@Injectable()
@RestParams({
    url: BaseService.BASE_URL + '/v1/courses'
})
export class CourseService extends BaseService {
    @RestAction({
        method: RestRequestMethod.Post,
    })
    save: IRestMethod<any, any>;

    @RestAction({
        method: RestRequestMethod.Put,
        path: '/{!id}'
    })
    update: IRestMethod<any, any>;

    @RestAction({
        path: '/{!id}'
    })
    get: IRestMethod<any, any>;

    @RestAction({
        isArray: true
    })
    query: IRestMethod<any, any[]>;

    @RestAction({
        method: RestRequestMethod.Delete,
        path: '/{!id}'
    })
    remove: IRestMethod<{ id: any }, any>;

    @RestAction({
        method: RestRequestMethod.Post,
        path: '/{!id}/disciplines'
    })
    addDisciplines: IRestMethod<any, any>;

    @RestAction({
        method: RestRequestMethod.Delete,
        path: '/{!id}/disciplines/{!discipline_id}'
    })
    removeDiscipline: IRestMethod<any , any>;
}