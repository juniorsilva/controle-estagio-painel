import { LocalStorageService } from 'ngx-webstorage';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import {
    HttpErrorResponse,
    HttpEvent,
    HttpHandler,
    HttpInterceptor,
    HttpRequest,
    HttpResponse
} from '@angular/common/http';
import { SpinnerHelper } from '../spinner/spinner.helper';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import sjcl from 'sjcl';

@Injectable()
export class AuthClientHelper implements HttpInterceptor {
    isLogged: BehaviorSubject<any> = new BehaviorSubject<any>(false);
    user: BehaviorSubject<any> = new BehaviorSubject<any>(this.getUser());
    public userTypes = {'0': 'Administrador', '1': 'Professor'};

    constructor(private storage: LocalStorageService) {
    }

    extendHeaders(headers) {
        const user = this.getUser();
        if (user) {
            headers.Authorization = 'Bearer ' + user.access_token;
        }
        return headers;
    }

    isGuest() {
        return null === this.getUser();
    }

    setUserStatus(val) {
        this.isLogged.next(val);
    }

    setUser(user) {
        this.user.next(user);
        this.isLogged.next(true);
        this.storage.store('user', sjcl.encrypt('123', JSON.stringify(user), {}, {}).replace(/,/g, ',\n'));
    }

    clear() {
        this.user.next([]);
        this.isLogged.next(false);
        this.storage.clear();
    }

    getUser() {
        if (this.storage.retrieve('user') !== null) {
            return JSON.parse(sjcl.decrypt('123', this.storage.retrieve('user')));
        }
        return null;
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).do((event: HttpEvent<any>) => {
            if (event instanceof HttpResponse) {
                SpinnerHelper.displayLoader(false);
            } else {
                SpinnerHelper.displayLoader(true);
            }
        }, (err: any) => {
            if (err instanceof HttpErrorResponse) {
                if (err.status === 401) {
                }
            }
        });
    }
}
